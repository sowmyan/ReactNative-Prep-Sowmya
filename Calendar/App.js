/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Text,
    StyleSheet,
    ScrollView
} from 'react-native';
import {Calendar} from 'react-native-calendars';
import {LocaleConfig} from 'react-native-calendars';

LocaleConfig.locales['fr'] = {
    monthNames: ['Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'],
    dayNamesShort: ['Sun','Mon','Tue','Wed','Thu','Fri','Sat']
};

LocaleConfig.defaultLocale = 'fr';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.onDayPress = this.onDayPress.bind(this);
        this.onDayPressTwo =()=> {
            alert("you have clicked ");
        };
    }

    render() {
        return (
            <ScrollView style={styles.container}>
              <Text style={styles.text}>Calendar with selectable date and arrows</Text>
              <Calendar
                  onDayPress={this.onDayPress}
                  current={'2017-10-01'}
                  style={styles.calendar}
                  markedDates={{[this.state.selected]: {selected: true}}}
                  theme={{
                      calendarBackground: '#F0F8FF',
                      monthTextColor: '#000000',
                      textSectionTitleColor: '#0000CD',
                      dayTextColor: '#A350FB',
                  }}
              />
              <Text style={styles.text}>Calendar with marked dates and hidden arrows</Text>
              <Calendar
                  style={styles.calendar}
                  current={'2017-10-09'}
                  minDate={'2017-10-07'}
                  maxDate={'2017-10-28'}
                  onDayPress={this.onDayPressTwo }
                  firstDay={1}
                  markedDates={{
                      '2017-10-19': {selected: true, marked: true},
                      '2017-10-20': {marked: true},
                      '2017-10-28': {disabled: true}
                  }}
                  hideArrows={true}
                  theme={{
                      calendarBackground: '#F5FFFA',
                      monthTextColor: '#663399',
                      textSectionTitleColor: '#A350FB',
                      dayTextColor: '#A350FB',
                  }}
              />
              <Text style={styles.text}>Calendar with marked dates and spinner</Text>
              <Calendar
                  style={styles.calendar}
                  current={'2017-10-09'}
                  onDayPress={this.onDayPress}
                  minDate={'2017-10-03'}
                  displayLoadingIndicator
                  markingType={'interactive'}
                  theme={{
                      calendarBackground: '#708090',
                      textSectionTitleColor: 'white',
                      dayTextColor: 'white',
                      todayTextColor: 'white',
                      selectedDayTextColor: 'white',
                      monthTextColor: 'white',
                      selectedDayBackgroundColor: '#333248',
                      arrowColor: 'white',
                      'stylesheet.calendar.header': {
                          week: {
                              marginTop: 5,
                              flexDirection: 'row',
                              justifyContent: 'space-between'
                          }
                      }
                  }}
                  markedDates={{
                      '2017-10-08': [{textColor: '#666'}],
                      '2017-10-01': [{startingDay: true,color: '#EE82EE'}],
                      '2017-10-31': [{endingDay: true,color: '#EE82EE'}],
                      '2017-10-20': [{startingDay: true, color: 'green'}, {endingDay: true, color: 'blue'}],
                      '2017-10-21': [{startingDay: true, color: '#40E0D0'}],
                      '2017-10-22': [{ color: '#40E0D0'}],
                      '2017-10-23': [{ color: '#40E0D0'}],
                      '2017-10-24': [{ endingDay: true, color: '#40E0D0'}],
                  }}
                  hideArrows={false}
              />
            </ScrollView>
        );
    }

    onDayPress(day) {
        this.setState({
            selected: day.dateString
        });
    }
}

const styles = StyleSheet.create({
    calendar: {
        borderTopWidth: 1,
        paddingTop: 5,
        borderBottomWidth: 1,
        borderColor: '#eee',
        height: 350
    },
    text: {
        textAlign: 'center',
        borderColor: '#bbb',
        padding: 10,
        backgroundColor: '#eee'
    },
    container: {
        flex: 1,
        backgroundColor: 'gray'
    }
});
