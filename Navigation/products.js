import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

export default class  Products extends Component {
    static navigationOptions = {
        title: 'products',
    };
    render() {
        const { params } = this.props.navigation.state;
        return (
            <View>
                <Text>Hello products</Text>
                <Text>{params.cat}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({

});

AppRegistry.registerComponent('Products', () => Products);
