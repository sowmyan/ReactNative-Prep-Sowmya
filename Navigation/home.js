import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View, Button
} from 'react-native';

export default class Home extends Component {
    static navigationOptions = {
        title: 'Welcome',
    };
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Text>Hello from home</Text>
                <Button title = 'category 1' color="green"
                    onPress={() => navigate('Products', {cat:'category one'})}

                />


                <Button title = 'category 2' color="black"
                        onPress={() => navigate('Products', {cat:'category two'})}

                />


            </View>
        );
    }
}

const styles = StyleSheet.create({

    container : {
        display :'flex',
        alignItems:'center',
        justifyContent:'center',

    }

});

AppRegistry.registerComponent('Home', () => Home);
