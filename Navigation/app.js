import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Home from './home'
import  Products from './products'


 const Navigation = StackNavigator({
    Home: { screen: Home },
     Products: { screen: Products },
});


export default Navigation;