/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet, View
} from 'react-native';
import SwitchExample  from './home'




class Sample extends Component {

    render() {
        return (
            <View>
              <SwitchExample />
            </View>
        )
    }
}
export default Sample

AppRegistry.registerComponent('Sample', () => Sample);