import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

//import List from './List'

const PresentationComponent = (props) => {
    return (
        <View>
            <Text style = {styles.myState}>
                {props.myState}
            </Text>
            <Text style = {styles.bodyColor}>
                {props.bodytext}
            </Text>
        </View>
    )
}
export default PresentationComponent

const styles = StyleSheet.create ({
    myState: {
        marginTop: 20,
        textAlign: 'center',
        color: 'black',
        fontWeight: 'bold',
        fontSize: 20
    },
    bodyColor :{
        alignItems:'center',
        color:'black',
        fontSize: 29
    }
})
