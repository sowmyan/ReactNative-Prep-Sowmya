/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import  PresentationComponent from './StyleComponent'
class Styles extends Component {
    state = {
        myState: 'This is state',
        bodytext: 'This is coming from the component using props'
    }
    render() {
        return (
            <View>
              <PresentationComponent   myState = {this.state.myState}/>
              <PresentationComponent  myState = {this.state.bodytext}/>
            </View>
        )
    }
}AppRegistry.registerComponent('Styles', () => Styles);
