import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';
const PresentationalComponent = (props) => {
    return (
        <View style = {styles.container}>
            <View style = {styles.redbox} />
            <View style = {styles.bluebox} />
            <View style = {styles.blackbox} />
        </View>
    )
}
export default PresentationalComponent

const styles = StyleSheet.create ({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        justifyContent :'space-between',
        backgroundColor: 'gray',
        height: 700,
        alignItems :'flex-end'
    },
    redbox: {
        width: 100,
        height: 100,
        backgroundColor: 'white'
    },
    bluebox: {
        width: 150,
        height: 150,
        backgroundColor: 'blue'
    },
    blackbox: {
        width: 200,
        height: 200,
        backgroundColor: 'black'
    },
})


