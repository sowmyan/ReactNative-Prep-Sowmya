/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import PresentationalComponent from './presentationalcomponent'
class Flexbox extends Component {
    state = {
        myState: 'This is state',

    }
    render() {
        return (
            <View>
              <PresentationalComponent myState = {this.state.myState}/>

            </View>

        )
    }
}
export default Flexbox


AppRegistry.registerComponent('Flexbox', () => Flexbox);
