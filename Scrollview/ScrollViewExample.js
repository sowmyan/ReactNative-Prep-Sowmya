import React, { Component } from 'react';
import { Text, Image, View, StyleSheet, ScrollView } from 'react-native';

class ScrollViewExample extends Component {
    state = {
        names: [
            {'name': 'priya', 'id': 1},
            {'name': 'Susan', 'id': 2},
            {'name': 'Ravi', 'id': 3},
            {'name': 'sam', 'id': 4},
            {'name': 'sarada', 'id': 5},
            {'name': 'Laura', 'id': 6},
            {'name': 'gopi', 'id': 7},
            {'name': 'Debra', 'id': 8},
            {'name': 'Aron', 'id': 9},
            {'name': 'Ann', 'id': 10},
            {'name': 'Steve', 'id': 11},
            {'name': 'Olivia', 'id': 12}
        ]
    }
    render() {
        return (
            <View>
                <Image style= {styles.image}  source = {require('./android/app/src/img/image.jpeg')} />
                <ScrollView>
                    {
                        this.state.names.map((item, index) => (
                        <View key = {item.id} style = {styles.item}>
                            <Text>{item.name}  </Text>
                        </View>
                        ))
                    }
                </ScrollView>
            </View>
        )
    }
}
export default ScrollViewExample

const styles = StyleSheet.create ({
    item: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 30,
        margin: 5,
        borderColor: '#2a4944',
        borderWidth: 1,
        backgroundColor: '#d2f7f1'
    },

    image : {
        justifyContent : 'center',
        flexDirection: 'row',

        alignItems: 'stretch',

    }
})