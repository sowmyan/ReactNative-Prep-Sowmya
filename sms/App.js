/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react'
import { StyleSheet, Button, View } from 'react-native'
import SendSMS from 'react-native-sms'


export default class App extends React.Component {
    render() {
        return (
            <View >
                <Button title="Send sms" onPress={this.someFunction}/>
            </View>
        )
    }
    someFunction() {

        SendSMS.send({
            body: ' ',
            recipients: ['0123456789', '9876543210'],
            successTypes: ['sent', 'queued']
        }, (completed, cancelled, error) => {

            console.log('SMS Callback: completed: ' + completed + ' cancelled: ' + cancelled + 'error: ' + error);

        });
    }
}
