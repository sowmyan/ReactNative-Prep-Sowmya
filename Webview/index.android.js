/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import WebViewExample from './Webexample.js'

export default class Webview extends Component {
  render() {
    return (
     < WebViewExample/>
    );
  }
}


AppRegistry.registerComponent('Webview', () => Webview);
