/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Home from './home'
export default class Pickerapp1 extends Component {
    state = {
        myState: 'This is state',

    }
    render() {
        return (
            <View>
              <Home myState = {this.state.myState}/>

            </View>
        )
    }
}
AppRegistry.registerComponent('Pickerapp1', () => Pickerapp1);
